use std::cell::RefCell;
use std::rc::Rc;
use std::sync::mpsc::{channel, Receiver, Sender};

use crate::components::dialog::dialog_msg::DialogMsg;
use crate::components::dialog::dialog_state::DialogState;
use crate::patterns::{Controller, Uid};
use crate::rr_switchboard::RrSwitchboard;
use crate::rrc::Rrc;
use crate::switchboard::{Switchboard, TickMsg, TickMsgType, TickSender};

use log::debug;

pub struct DialogController {
    uid: Uid,
    state: DialogState,
    sender: Sender<DialogMsg>,
    receiver: Receiver<DialogMsg>,
    tick_sender: TickSender,
}

impl DialogController {
    pub fn new(switchboard: RrSwitchboard, num_options: usize) -> Rrc<DialogController> {
        let (sender, receiver) = channel::<DialogMsg>();

        let rrc = Rrc::new(DialogController {
            uid: Uid::new(),
            state: DialogState::new(num_options),
            sender,
            receiver,
            tick_sender: switchboard.borrow().get_tick_request_sender().clone(),
        });

        switchboard.borrow_mut().insert_controller(rrc.to_any());

        rrc
    }
}

impl Controller for DialogController {
    fn uid(&self) -> Uid {
        self.uid
    }

    fn wants_tick(&self) -> bool {
        unimplemented!()
    }

    fn tick(&mut self) -> bool {
        unimplemented!()
    }

    fn desc(&self) -> String {
        format!("DialogController")
    }
}

impl Drop for DialogController {
    fn drop(&mut self) {
        self.tick_sender
            .send(TickMsg::new(TickMsgType::Drop, self.uid))
            .map_err(|err| {
                log::debug!(
                    "Dropping DialogController failed to inform Switchboard, because {}",
                    err
                )
            });
    }
}
