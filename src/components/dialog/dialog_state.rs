use crate::components::dialog::dialog_msg::DialogMsg;
use crate::patterns::State;

#[derive(Clone, Copy, Debug)]
pub struct DialogState {
    selected: Option<usize>,
    highlighted: usize,
    num_options: usize,
}

impl DialogState {
    pub fn new(num_options: usize) -> Self {
        // debug_assert!(highlighted < num_options);
        DialogState {
            selected: None,
            highlighted: 0,
            num_options,
        }
    }
}

impl State for DialogState {
    type Msg = DialogMsg;

    fn update(&mut self, msg: DialogMsg) -> bool {
        debug_assert!(!self.is_accepting());
        debug_assert!(self.highlighted < self.num_options);

        match msg {
            DialogMsg::Next => {
                self.highlighted = (self.highlighted + 1) % self.num_options;
                true
            }
            DialogMsg::Prev => {
                self.highlighted = (self.highlighted + 1) % self.num_options;
                true
            }
            DialogMsg::ChooseSelected => {
                self.selected = Some(self.highlighted);
                true
            }
        }
    }

    fn is_accepting(&self) -> bool {
        self.selected.is_none()
    }
}
