use std::fmt;
use std::rc::Rc;
use std::sync::Arc;

use crate::components::dialog::dialog_controller::DialogController;
use crate::components::dialog::dialog_state::DialogState;
use crate::components::puzzles::button::draw_button;
use crate::components::puzzles::button_state::ButtonStateEnum;
use crate::components::puzzles::frame::{draw_frame, FrameStyle};
use crate::globals::style_provider::{FocusType, StyleProvider};
use crate::io::keys::Key;
use crate::io::output::Output;
use crate::io::style::TextStyle;
use crate::primitives::rect::Rect;
use crate::primitives::texts::line_screen_width;
use crate::primitives::xy::XY;
use crate::rr_switchboard::RrSwitchboard;
use crate::rrc::Rrc;
use crate::rrv::Rrv;
use crate::view::view::View;
use std::cmp::max;
use unicode_segmentation::UnicodeSegmentation;

static MIN_SIZE: XY = XY::new(10, 3);
static MAX_BUTTON_WIDTH: u16 = 14;
static HORIZONTAL_DIST: u16 = 2;

pub struct DialogOption {
    caption: String,
}

impl DialogOption {
    pub fn new(caption: String) -> DialogOption {
        DialogOption { caption }
    }
}

pub type Options = Vec<DialogOption>;

struct CachedRects {
    pub text: Rect,
    pub buttons: Vec<Rect>,
}

pub struct DialogView {
    current_size: XY,
    ctrl: Rrc<DialogController>,
    text: String,
    options: Options,
    switchboard: RrSwitchboard,
}

impl DialogView {
    pub fn new(switchboard: RrSwitchboard, text: String, options: Options) -> Rrv<DialogView> {
        let ctrl = DialogController::new(switchboard.clone(), options.len());

        let view = DialogView {
            current_size: MIN_SIZE,
            ctrl,
            text,
            options,
            switchboard,
        };

        Rrv::new(view)
    }
}

impl View for DialogView {
    fn desc(&self) -> Arc<String> {
        Arc::new(format!("Dialog with {} options.", self.options.len()))
    }

    fn min_size(&self) -> XY {
        let mut x = 0;
        let mut y = 3;

        for line in self.text.lines().into_iter() {
            y += 1;
            x = max(x, line_screen_width(line));
        }
        x += 2 * HORIZONTAL_DIST;

        let buttons_width: u16 = ((self.options.len() as u16) * MAX_BUTTON_WIDTH
            + (self.options.len() as u16 + 1) * HORIZONTAL_DIST);

        if x < buttons_width {
            x = buttons_width
        }

        (x, y).into()
    }

    fn get_size(&self) -> XY {
        self.current_size
    }

    fn set_size(&mut self, new_size: XY) {
        self.current_size = new_size;
    }

    fn accepts_input(&self) -> bool {
        true
    }

    fn consume_input(&mut self, key: Key) -> bool {
        match key {
            Key::Esc => true,
            Key::Enter => true,
            _ => false,
        }
    }

    fn render(
        &self,
        style_provider: &StyleProvider,
        focus_type: FocusType,
        output: &mut dyn Output,
    ) {
        let style = style_provider.for_text(focus_type);

        draw_frame(style_provider, focus_type, FrameStyle::Roman, output);

        let mut max_y: u16 = 0;
        for (y, line) in self.text.lines().enumerate() {
            output.print_at((HORIZONTAL_DIST, 1 + y as u16).into(), style, line);
            max_y = max(max_y, y as u16);
        }

        let mut op_pos: XY = (HORIZONTAL_DIST, max_y + 2).into();

        for op in self.options.iter() {
            draw_button(
                style_provider,
                focus_type,
                output,
                op_pos,
                &op.caption,
                MAX_BUTTON_WIDTH,
                ButtonStateEnum::Available,
            );

            op_pos = op_pos + XY::new(MAX_BUTTON_WIDTH + HORIZONTAL_DIST, 0);
        }
    }
}
