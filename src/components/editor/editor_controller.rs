use std::cell::{Cell, RefCell};
use std::rc::Rc;
use std::sync::mpsc::{channel, Receiver, Sender, TryRecvError};

use log::error;

use crate::components::editor::editor_msg::EditorMsg;
use crate::components::editor::editor_pilot::EditorPilot;
use crate::components::editor::editor_state::EditorState;
use crate::patterns::{Controller, State, Uid};
use crate::rr_switchboard::RrSwitchboard;
use crate::rrc::Rrc;
use crate::switchboard::{Switchboard, TickSender};

pub struct EditorController {
    uid: Uid,
    state: EditorState,
    sender: Sender<EditorMsg>,
    receiver: Receiver<EditorMsg>,
    tick_sender: TickSender,
}

impl EditorController {
    pub fn new(switchboard: RrSwitchboard) -> Rrc<EditorController> {
        let (sender, receiver) = channel::<EditorMsg>();

        let tick_sender = switchboard.borrow().get_tick_request_sender().clone();

        let rrc = Rrc::new(EditorController {
            uid: Uid::new(),
            state: EditorState::new(),
            sender,
            receiver,
            tick_sender,
        });

        switchboard.borrow_mut().insert_controller(rrc.to_any());

        rrc
    }

    pub fn pilot(&self) -> EditorPilot {
        EditorPilot::new(self.uid, self.tick_sender.clone(), self.sender.clone())
    }

    pub fn state(&self) -> &EditorState {
        &self.state
    }
}

impl Controller for EditorController {
    fn uid(&self) -> Uid {
        self.uid
    }

    fn wants_tick(&self) -> bool {
        unimplemented!()
    }

    fn tick(&mut self) -> bool {
        match self.receiver.try_recv() {
            Ok(msg) => {
                self.state.update(msg);
                true
            }
            Err(TryRecvError::Empty) => false,
            Err(e) => {
                error!("EditorController error {:?}", e);
                false
            }
        }
    }

    fn desc(&self) -> String {
        format!("EditorController")
    }
}
