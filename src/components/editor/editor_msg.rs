//TEMPORARY
#[derive(PartialEq, Eq, Clone, Debug)]
pub enum EditorMsg {
    PreQuit,
    AddLetter(char),
    Save,
}
