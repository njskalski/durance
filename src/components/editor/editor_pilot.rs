use std::sync::mpsc::Sender;

use log::debug;

use crate::components::editor::editor_msg::EditorMsg;
use crate::patterns::{Pilot, Uid};
use crate::switchboard::{TickMsg, TickMsgType, TickSender};

#[derive(Clone)]
pub struct EditorPilot {
    sender: Sender<EditorMsg>,
    tick_sender: TickSender,
    uid: Uid,
}

impl EditorPilot {
    pub fn new(uid: Uid, tick_sender: TickSender, sender: Sender<EditorMsg>) -> Self {
        EditorPilot {
            sender,
            tick_sender,
            uid,
        }
    }
}

impl Pilot for EditorPilot {
    type Msg = EditorMsg;

    fn send(&self, msg: EditorMsg) -> bool {
        if self.sender.send(msg).is_err() {
            return false;
        }

        if self
            .tick_sender
            .send(TickMsg::new(TickMsgType::Tick, self.uid))
            .is_err()
        {
            return false;
        }
        debug!("requesting tick for {}", self.uid);
        true
    }
}
