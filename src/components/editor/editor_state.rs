use std::ops::Add;

use log::{debug, error};

use crate::components::editor::editor_msg::EditorMsg;
use crate::io::keys::Key;
use crate::io::keys_consumer::KeysConsumeResult;
use crate::io::output::Output;
use crate::patterns::State;
use crate::primitives::xy::XY;
use crate::view::view::View;

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub enum OverlayDialog {
    None,
    SaveBeforeQuitting,
}

#[derive(PartialEq, Eq, Clone, Debug)]
pub struct EditorState {
    // TODO this is a mock, text data is not part of View, so it won't be in ViewState
    text: String,
    //TODO this is also a mock.
    overlay_dialog: OverlayDialog,
    has_changes: bool, //TODO this is to be moved into buffer state.
}

impl EditorState {
    pub fn new() -> Self {
        EditorState {
            text: "".to_owned(),
            overlay_dialog: OverlayDialog::None,
            has_changes: false,
        }
    }

    pub fn text(&self) -> &String {
        &self.text
    }

    pub fn has_overlay(&self) -> bool {
        self.overlay_dialog != OverlayDialog::None
    }

    pub fn overlay(&self) -> OverlayDialog {
        self.overlay_dialog
    }
}

impl State for EditorState {
    type Msg = EditorMsg;

    fn update(&mut self, edit_event: EditorMsg) -> bool {
        // debug_assert!(self.is_accepting());
        match edit_event {
            EditorMsg::PreQuit => {
                if self.overlay_dialog != OverlayDialog::None {
                    debug!("TODO no sure what to do, pre-quit while non accepting.")
                }

                self.overlay_dialog = OverlayDialog::SaveBeforeQuitting;
                true
            }
            EditorMsg::AddLetter(l) => {
                if self.overlay_dialog != OverlayDialog::None {
                    debug!("Weird, adding letters to a blocked EditorView")
                }

                self.text.push(l);
                self.has_changes = true;
                true
            }
            EditorMsg::Save => {
                debug!("actual saving not implemented, mocking it to if dialogs work");
                self.has_changes = false;
                true
            }
            _ => {
                error!("unhandled msg {:?}", edit_event);
                false
            }
        }
    }
    fn is_accepting(&self) -> bool {
        self.overlay_dialog == OverlayDialog::None
    }
}
