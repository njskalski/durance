use std::borrow::Borrow;
use std::cell::{Cell, RefCell, RefMut};
use std::rc::Rc;
use std::sync::Arc;

use crossbeam_channel::Sender;
use log::debug;

use crate::components::dialog::dialog_view::{DialogOption, DialogView};
use crate::components::editor::editor_controller::EditorController;
use crate::components::editor::editor_msg::EditorMsg;
use crate::components::editor::editor_pilot::EditorPilot;
use crate::components::editor::editor_state::{EditorState, OverlayDialog};
use crate::globals::style_provider::{background_of, FocusType, StyleProvider};
use crate::io::keys::Key;
use crate::io::keys_consumer::KeysConsumeResult;
use crate::io::output::Output;
use crate::io::style::TextStyle;
use crate::patterns::{Pilot, State};
use crate::primitives::rect::Rect;
use crate::primitives::xy::XY;
use crate::rr_switchboard::RrSwitchboard;
use crate::rrc::Rrc;
use crate::rrv::{AnyView, Rrv};
use crate::view::view::View;

// completely arbitrary
const MIN_SIZE: XY = XY::new(10, 5);

pub struct EditorView {
    current_size: XY,
    ctrl: Rrc<EditorController>,
    pilot: EditorPilot,
    switchboard: RrSwitchboard,
    subview: RefCell<Option<Rrv<DialogView>>>,
}

impl EditorView {
    pub fn new(switchboard: RrSwitchboard, controller: Rrc<EditorController>) -> Self {
        let pilot = controller.borrow().pilot();

        EditorView {
            current_size: MIN_SIZE,
            ctrl: controller,
            pilot,
            switchboard,
            subview: RefCell::new(None),
        }
    }

    fn get_dialog(&self) -> Rrv<DialogView> {
        if self.subview.borrow().is_some() {
            return self.subview.borrow().clone().unwrap();
        }

        let options: Vec<DialogOption> = vec![
            DialogOption::new("op1".to_string()),
            DialogOption::new("op2dfsafdsafasdfadfasdfasdfasfd".to_string()),
            DialogOption::new("cancel".to_string()),
        ];

        let dialog = DialogView::new(
            self.switchboard.clone(),
            "bla bla bla\ntwojastara".to_owned(),
            options,
        );

        self.subview.replace(Some(dialog.clone()));

        dialog
    }
}

impl View for EditorView {
    fn desc(&self) -> Arc<String> {
        Arc::new("Editor".to_owned()) // TODO
    }

    fn min_size(&self) -> XY {
        MIN_SIZE
    }

    fn get_size(&self) -> XY {
        self.current_size
    }
    fn set_size(&mut self, new_size: XY) {
        self.current_size = new_size;
    }
    fn accepts_input(&self) -> bool {
        true //TODO
    }

    fn consume_input(&mut self, key: Key) -> bool {
        debug_assert!(self.accepts_input());
        // debug!("editor view consuming {:?}", key);
        match key {
            Key::Letter(l) => {
                // self.state.update(EditorMsg::AddLetter(l)).into()
                self.pilot.send(EditorMsg::AddLetter(l));
                true
            }
            Key::CtrlLetter('s') => {
                self.pilot.send(EditorMsg::Save);
                true
            }
            // TODO move it somewhere
            Key::CtrlLetter('w') => {
                self.pilot.send(EditorMsg::PreQuit);
                true
            }
            _ => false,
        }
    }

    fn render(
        &self,
        style_provider: &StyleProvider,
        focus_type: FocusType,
        output: &mut dyn Output,
    ) {
        let ctrl_ref = self.ctrl.borrow();
        let state = ctrl_ref.state();

        if !state.has_overlay() {
            let style = style_provider.for_text(focus_type);

            output.print_at((0, 0).into(), style, state.text());
        } else {
            let style = style_provider.for_text(background_of(focus_type));

            output.print_at((0, 0).into(), style, state.text());

            match state.overlay() {
                OverlayDialog::None => {}
                OverlayDialog::SaveBeforeQuitting => {
                    self.get_dialog()
                        .borrow_mut()
                        .render(style_provider, focus_type, output)
                }
            }
        }
    }
}
