use crate::components::puzzles::button_state::ButtonStateEnum;
use crate::components::puzzles::frame::FrameStyle;
use crate::globals::style_provider::{FocusType, StyleProvider};
use crate::io::output::Output;
use crate::primitives::xy::XY;
use unicode_segmentation::UnicodeSegmentation;
use unicode_width::UnicodeWidthStr;

const HORIZONTAL_MARGIN: u16 = 1;

pub fn draw_button(
    style_provider: &StyleProvider,
    ft: FocusType,
    output: &mut dyn Output,
    pos: XY,
    text: &str,
    width: u16,
    state: ButtonStateEnum,
) {
    let style_text = style_provider.for_buttons(ft, state);

    output.print_at(pos, style_text, "[");
    output.print_at(pos + (width - 1, 0), style_text, "]");

    let mut cur_pos: XY = XY::new(2, 0);
    let mut needs_ellipsis = false;

    let text_limit = width - 2 * HORIZONTAL_MARGIN;

    for g in text.graphemes(true) {
        if cur_pos.x + (g.width() as u16) > text_limit {
            needs_ellipsis = true;
            break;
        }

        output.print_at(pos + cur_pos, style_text, g);
        cur_pos = cur_pos + XY::new(g.width() as u16, 0);
    }

    if needs_ellipsis {
        output.print_at(pos + cur_pos, style_text, "…");
    }
}
