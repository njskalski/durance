use crate::globals::style_provider::{FocusType, StyleProvider};
use crate::io::output::Output;
use crate::io::sub_output::SubOutput;
use crate::primitives::rect::Rect;

use log::debug;

#[derive(Copy, Clone, Debug)]
pub enum FrameStyle {
    Thin,
    Double,
    Roman,
}

enum FramePart {
    Vertical,
    Horizontal,
    CornerLowerLeft,
    CornerLowerRight,
    CornerUpperLeft,
    CornerUpperRight,
}

pub fn draw_frame(
    style_provider: &StyleProvider,
    ft: FocusType,
    frame_style: FrameStyle,
    output: &mut dyn Output,
) {
    let style = style_provider.for_frames(ft);

    debug!("drawing frame {:?}", output.size());

    for x in 1..output.size().x - 1 {
        output.print_at(
            (x, 0).into(),
            style,
            get_frame_part(FramePart::Horizontal, frame_style),
        );
        output.print_at(
            (x, output.size().y - 1).into(),
            style,
            get_frame_part(FramePart::Horizontal, frame_style),
        );
    }

    for y in 1..output.size().y - 1 {
        output.print_at(
            (0, y).into(),
            style,
            get_frame_part(FramePart::Vertical, frame_style),
        );
        output.print_at(
            (output.size().x - 1, y).into(),
            style,
            get_frame_part(FramePart::Vertical, frame_style),
        );
    }

    output.print_at(
        (0, 0).into(),
        style,
        get_frame_part(FramePart::CornerUpperLeft, frame_style),
    );
    output.print_at(
        (0, output.size().y - 1).into(),
        style,
        get_frame_part(FramePart::CornerLowerLeft, frame_style),
    );

    output.print_at(
        (output.size().x - 1, 0).into(),
        style,
        get_frame_part(FramePart::CornerUpperRight, frame_style),
    );
    output.print_at(
        (output.size().x - 1, output.size().y - 1).into(),
        style,
        get_frame_part(FramePart::CornerLowerRight, frame_style),
    );
}

fn get_frame_part(frame_part: FramePart, frame_style: FrameStyle) -> &'static str {
    match frame_part {
        FramePart::Horizontal => match frame_style {
            FrameStyle::Thin => "─",
            FrameStyle::Double => "═",
            FrameStyle::Roman => "─",
        },
        FramePart::Vertical => match frame_style {
            FrameStyle::Thin => "|",
            FrameStyle::Double => "║",
            FrameStyle::Roman => "║",
        },
        FramePart::CornerLowerLeft => match frame_style {
            FrameStyle::Thin => "╰",
            FrameStyle::Double => "╚",
            FrameStyle::Roman => "╙",
        },
        FramePart::CornerLowerRight => match frame_style {
            FrameStyle::Thin => "╯",
            FrameStyle::Double => "╝",
            FrameStyle::Roman => "╜",
        },
        FramePart::CornerUpperLeft => match frame_style {
            FrameStyle::Thin => "╭",
            FrameStyle::Double => "╔",
            FrameStyle::Roman => "╓",
        },
        FramePart::CornerUpperRight => match frame_style {
            FrameStyle::Thin => "╮",
            FrameStyle::Double => "╗",
            FrameStyle::Roman => "╖",
        },
    }
}
