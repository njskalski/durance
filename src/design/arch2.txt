So I came up with an update, there will be a rewrite.

So imagine a FilesystemView widget, where you want to navigate and possibly rename files.

There are two states in play here:
- Filesystem state (what files are there, reacting to external changes), and we don't change that.
- Widget state (where your cursor is, am I waiting for you to give me a new filename)

Now a widget composes it's visual (tree of Elems) from both. Possibly updates on different messages.

So here's new design:

View is something that:
- translates user input into messages and sends it to widget controller (that updates widget state)
- composes ElemTree from multiple controllers it holds references to.

That means that Controllers need to react to messages, possibly async, possibly triggering redraw.

So here's a bulletproof design (maybe overkill):

There is a family of Controllers, they react to messages and are possibly ticked. There will be a system that
keeps a track of controllers, watches if they are needed, ticks them etc.

States are something managed by controllers. A View queries controller for States it needs to draw itself.

Now where is the Widget (View?) hierarchy stored?
Nowhere really. If a View is in the state where it should draw a sub-view, it will:
1) ask it's Controller for WidgetState
2) figure out "oh, I'm in the state of showing subview X".
fuck.

I just created new layer of abstraction that solves nothing.
FUCK.