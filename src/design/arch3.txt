14 Nov

So to boil down two previous design experiments, here's what I do now:

In general View is a Controller of widget state (sometimes called ViewState). I see no reason to separate the two.
It represents a single View, that accepts USER input (and user input only).

Any State is Unmutable and Serializable.

Controllers will be for objects that have State but have no View.
A View can refer to other Controllers or "services" (singleton Controllers).

Non user input will be handled by Controllers/Services that have nothing to do with User input.

View that rely on such non-user inputs for their state will query these services as necessary.