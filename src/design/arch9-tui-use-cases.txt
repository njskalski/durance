One successful project I've been part of, made it's design by writing down all use cases and running them as
"tests" to design proposals on a whiteboard in an iterative process to figure out the class hierarchy.
Let's try again. Here are the use-cases of TUI I want:

1) Multiple focusable views. That is: if screen is broken into two pieces vertically, the one on the right is say
editor that's greyed and covered with save-as dialog, the one on the left should remain accepting input.
Dialog blocks only the context that is subject to it's output.

2) Input escalation. If a child view (say code completion) does not consume input, and there is an accepting parent
(editor under code completion), the input is escalated from front-most view further down.

3) Resizing is top-down. We have constraint on entire screen.

4) Context escalation. A context menu can move up from a single word in editor above which it was spawned, through
entire file editor context up to entire program context, with "escalate context" button.
Not sure if it will affect the design but it's so essential to planned experience I'd hate to discover it's impossible.

5) Within one window, we have multiple "stateful" objects, like in "save as dialog" buttons (that can be highlighted or
not) and file tree.

and small use cases:

6) Dialog ends, needs to inform parent on result.
Either child has mut-ref to parent (rc<refcell<T>>)
Or has a channel and sends message.
Or has a &'static to parent, and gets channel from there.

Anyway it seems like the channel is a way. If channel than messages. If messages than states and probably pilots for
convenience.