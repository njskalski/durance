pub trait TickingState {
    fn tick(&mut self);
}

struct StateA {
    parent: &'static dyn TickingState,
}
