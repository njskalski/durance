// To those of you who have no style, this will not work for you. Keep searching.

use crate::components::puzzles::button_state::ButtonStateEnum;
use crate::io::style::{Effect, TextStyle};
use crate::primitives::colour::Colour;

// these are some arbitrary colours I picked from pictures I like.

const FOCUSED_BG: Colour = (2, 7, 4);
const ALTERNATIVE_BG: Colour = (5, 20, 21);
const INACTIVE_BG: Colour = (3, 42, 43);

const DARK_RED: Colour = (32, 1, 9);
const BRIGHT_RED: Colour = (127, 21, 58);
const BG_BLUE: Colour = (3, 49, 65);
const BG_GREEN: Colour = (6, 30, 40);
const LEAF_BROWN: Colour = (69, 52, 44);
const SOME_PINK: Colour = (165, 109, 156);
const COOL_BLUE: Colour = (202, 218, 231);
const STONE_BLUE: Colour = (35, 37, 34);

pub struct StyleProvider {}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum FocusType {
    // Primary focus, accepting input.
    Hovered,
    // This is a view that's not in focus, but can be immediately switched to.
    Alternative,
    // This is a view that does not accept input.
    Inactive,
    // This is a view that's an ancestor of view that currently accepts input.
    OnPathButCovered,
}

pub fn background_of(ft: FocusType) -> FocusType {
    match ft {
        FocusType::Hovered => FocusType::OnPathButCovered,
        FocusType::Alternative => FocusType::Alternative,
        FocusType::Inactive => FocusType::Inactive,
        FocusType::OnPathButCovered => FocusType::OnPathButCovered,
    }
}

impl StyleProvider {
    pub fn new() -> Self {
        StyleProvider {}
    }

    pub fn for_text(&self, ft: FocusType) -> TextStyle {
        match ft {
            FocusType::Hovered => TextStyle::new(COOL_BLUE, FOCUSED_BG, Effect::None),
            FocusType::Alternative => TextStyle::new(STONE_BLUE, ALTERNATIVE_BG, Effect::None),
            FocusType::Inactive => TextStyle::new(LEAF_BROWN, INACTIVE_BG, Effect::None),
            FocusType::OnPathButCovered => TextStyle::new(DARK_RED, INACTIVE_BG, Effect::None),
        }
    }

    pub fn for_buttons(&self, ft: FocusType, button_state: ButtonStateEnum) -> TextStyle {
        TextStyle::black_and_white()
    }

    pub fn for_frames(&self, ft: FocusType) -> TextStyle {
        TextStyle::black_and_white()
    }
}
