use crate::io::keys::Key;

pub enum KeysConsumeResult {
    Ignored,
    Consumed,
}
