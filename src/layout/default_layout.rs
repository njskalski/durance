use crate::globals::style_provider::{FocusType, StyleProvider};
use crate::io::output::Output;
use crate::layout::layout::Layout;
use crate::primitives::rect::Rect;
use crate::primitives::xy::XY;
use crate::rrv::AnyView;
use crate::view::view::View;

pub struct DefaultLayout {
    view: AnyView,
}

impl DefaultLayout {
    pub fn new(view: AnyView) -> Self {
        DefaultLayout { view }
    }
}

impl Layout for DefaultLayout {
    fn render(
        &self,
        style_provider: &StyleProvider,
        focus_type: FocusType,
        output: &mut dyn Output,
    ) {
        self.view
            .borrow()
            .render(style_provider, focus_type, output);
    }

    fn active_view(&self) -> Option<AnyView> {
        self.view.clone().into()
    }

    fn resize(&mut self, new_size: XY) {
        self.view.borrow_mut().set_size(new_size)
    }
}
