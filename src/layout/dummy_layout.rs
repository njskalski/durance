use crate::globals::style_provider::{FocusType, StyleProvider};
use crate::io::output::Output;
use crate::io::style::TextStyle;
use crate::layout::layout::Layout;
use crate::primitives::rect::Rect;
use crate::primitives::xy::XY;
use crate::rrv::AnyView;
use crate::view::view::View;

pub struct DummyLayout {}

impl DummyLayout {
    pub fn new() -> Self {
        DummyLayout {}
    }
}

impl Layout for DummyLayout {
    fn render(
        &self,
        style_provider: &StyleProvider,
        focus_type: FocusType,
        output: &mut dyn Output,
    ) {
        let style = style_provider.for_text(focus_type);
        for x in 0..output.size().x {
            for y in 0..output.size().y {
                output.print_at((x, y).into(), style, "x");
            }
        }
    }

    fn active_view(&self) -> Option<AnyView> {
        None
    }

    fn resize(&mut self, new_size: XY) {
        unimplemented!()
    }
}
