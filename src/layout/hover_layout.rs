use log::error;

use crate::globals::style_provider::{background_of, FocusType, StyleProvider};
use crate::io::output::Output;
use crate::io::sub_output::SubOutput;
use crate::layout::layout::Layout;
use crate::primitives::rect::Rect;
use crate::primitives::xy::XY;
use crate::rrv::AnyView;
use crate::view::view::View;

pub struct HoverLayout {
    background_view: AnyView,
    foreground_view: AnyView,
    background_defocused: bool,
}

impl HoverLayout {
    pub fn new(
        background_view: AnyView,
        foreground_view: AnyView,
        background_defocused: bool,
    ) -> Self {
        HoverLayout {
            background_view,
            foreground_view,
            background_defocused,
        }
    }
}

impl Layout for HoverLayout {
    fn render(
        &self,
        style_provider: &StyleProvider,
        focus_type: FocusType,
        output: &mut dyn Output,
    ) {
        // TODO move centering somewhere else, support more options

        let sub_view_size = self.foreground_view.borrow().min_size();

        // TODO what with messed up options like x too big and y good enough.
        if sub_view_size >= output.size() {
            error!("background is completely covered");
            self.foreground_view
                .borrow()
                .render(style_provider, focus_type, output);
        } else {
            let background_focus_type = if self.background_defocused {
                background_of(focus_type)
            } else {
                focus_type
            };

            let pos_x = (output.size().x - sub_view_size.x) / 2;
            let pos_y = (output.size().y - sub_view_size.y) / 2;
            let pos = XY::new(pos_x, pos_y);

            self.background_view
                .borrow()
                .render(style_provider, background_focus_type, output);

            let mut constrained_output =
                SubOutput::new(Box::new(output), Rect::new(pos, sub_view_size));

            self.foreground_view.borrow().render(
                style_provider,
                focus_type,
                &mut constrained_output,
            )
        }
    }

    fn active_view(&self) -> Option<AnyView> {
        Some(self.foreground_view.clone())
    }

    fn resize(&mut self, new_size: XY) {
        self.background_view.borrow_mut().set_size(new_size);

        let smaller = self.foreground_view.borrow().min_size();
        self.foreground_view.borrow_mut().set_size(smaller);
    }
}
