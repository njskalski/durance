use crate::globals::style_provider::{FocusType, StyleProvider};
use crate::io::output::Output;
use crate::primitives::xy::XY;
use crate::rrv::AnyView;
use crate::view::view::View;

pub trait Layout {
    // renders always from (0,0). If you want to render from different point,
    // put your view into ScrollLayout.
    fn render(
        &self,
        style_provider: &StyleProvider,
        focus_type: FocusType,
        output: &mut dyn Output,
    );
    fn active_view(&self) -> Option<AnyView>;

    // this passes size data down through the LayoutTree to the Views.
    fn resize(&mut self, new_size: XY);
}
