use std::arch::x86_64::_mm_minpos_epu16;

use crate::globals::style_provider::{FocusType, StyleProvider};
use crate::io::output::Output;
use crate::layout::layout::Layout;
use crate::primitives::rect::Rect;
use crate::primitives::xy::XY;
use crate::rrv::AnyView;
use crate::view::view::View;

pub enum ScrollDirection {
    Horizontal,
    Vertical,
    Both,
}

struct ScrollLayout {
    direction: ScrollDirection,
    // I don't want to do scrolling of other layouts now
    view: AnyView,
    pos: XY,
}

impl Layout for ScrollLayout {
    fn render(
        &self,
        style_provider: &StyleProvider,
        focus_type: FocusType,
        output: &mut dyn Output,
    ) {
        unimplemented!()
    }

    fn active_view(&self) -> Option<AnyView> {
        self.view.clone().into()
    }

    fn resize(&mut self, new_size: XY) {
        //Scroll supresses the resize.
    }
}
