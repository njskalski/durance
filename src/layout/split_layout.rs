use std::borrow::Borrow;
use std::cell::Cell;
use std::collections::BTreeMap;
use std::process::id;

use log::debug;

use crate::globals::style_provider::{FocusType, StyleProvider};
use crate::io::output::Output;
use crate::io::sub_output::SubOutput;
use crate::layout::layout::Layout;
use crate::primitives::rect::Rect;
use crate::primitives::xy::XY;
use crate::rrv::AnyView;
use crate::view::view::View;

pub enum SplitDirection {
    Horizontal,
    Vertical,
}

pub struct SplitLayout {
    // proportion, layout
    nodes: Vec<(u16, Box<dyn Layout>)>,
    direction: SplitDirection,
    active: usize,
    size: XY,
    layout_rects: Vec<Rect>,
}

impl SplitLayout {
    pub fn new(
        size: XY,
        direction: SplitDirection,
        nodes: Vec<(u16, Box<dyn Layout>)>,
        active: usize,
    ) -> Self {
        if log::log_enabled!(log::Level::Debug) {
            for (key, item) in nodes.iter() {
                debug_assert!(*key > 0);
            }
        }

        let mut res = SplitLayout {
            nodes,
            direction,
            active,
            size: (0, 0).into(), // just so early exit on resize below don't fail
            layout_rects: vec![],
        };

        // this recomputes layout rects and then populates data downwards.
        res.resize(size);

        res
    }

    fn recompute_layout_rects(&mut self) {
        let frame = self.size;
        let sum = self.nodes.iter().fold(0, |acc, item| acc + item.0);
        let mut props = vec![0 as u16; self.nodes.len()];

        let param = match self.direction {
            SplitDirection::Horizontal => frame.y,
            SplitDirection::Vertical => frame.x,
        };

        let mut filled = 0 as u16;
        let mut smallest = 0;
        for (idx, (prop, _)) in self.nodes.iter().enumerate() {
            props[idx] = ((*prop) * param) / sum;
            filled += props[idx];

            if props[idx] < props[smallest] {
                smallest = idx;
            }
        }
        debug!("props {:?} param {} filled {}", props, param, filled);

        //this is paranoid (and turned out to be necessary :D)
        debug_assert!(filled <= param);

        // TODO something more fair? just dump remainder to smallest.
        props[smallest] += param - filled;

        // let mut result: Vec<Rect> = vec![];
        self.layout_rects.clear();
        let result = &mut self.layout_rects;

        for idx in 0..self.nodes.len() {
            let rect: Rect = if idx == 0 {
                match self.direction {
                    SplitDirection::Horizontal => {
                        ((0, 0).into(), (frame.x, props[0]).into()).into()
                    }
                    SplitDirection::Vertical => ((0, 0).into(), (props[0], frame.y).into()).into(),
                }
            } else {
                let prev = result[idx - 1];
                match self.direction {
                    SplitDirection::Horizontal => (
                        (0, prev.lower_right().y).into(),
                        (frame.x, props[idx]).into(),
                    )
                        .into(),
                    SplitDirection::Vertical => (
                        (prev.lower_right().x, 0).into(),
                        (props[idx], frame.y).into(),
                    )
                        .into(),
                }
            };

            result.push(rect);
        }

        debug!("rects : {:?}", result);
    }
}

impl Layout for SplitLayout {
    fn render(
        &self,
        style_provider: &StyleProvider,
        focus_type: FocusType,
        output: &mut dyn Output,
    ) {
        let rects = &self.layout_rects;

        debug_assert!(rects.len() == self.nodes.len());

        for idx in 0..self.nodes.len() {
            // debug!("rendering {} at {}", idx, rects[idx]);
            self.nodes[idx].1.render(
                style_provider,
                focus_type,
                &mut SubOutput::new(Box::new(output), rects[idx]),
            );
        }
    }

    fn active_view(&self) -> Option<AnyView> {
        debug_assert!(self.active < self.nodes.len());
        self.nodes[self.active].1.active_view()
    }

    fn resize(&mut self, new_size: XY) {
        if new_size == self.size {
            debug!("new_size == old_size, dropping.");
            return;
        }

        self.size = new_size;
        self.recompute_layout_rects();

        for (idx, node_pair) in self.nodes.iter_mut().enumerate() {
            let rect = self.layout_rects[idx];
            node_pair.1.resize(rect.size);
        }
    }
}
