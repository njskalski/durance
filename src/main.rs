#[macro_use]
extern crate lazy_static;

use std::borrow::{Borrow, BorrowMut};
use std::io::{stdin, stdout, Read, Write};
use std::time::{Duration, Instant};

use crossbeam_channel::RecvError;
use log::{debug, Level, LevelFilter};
use syntect::highlighting::{Style, ThemeSet};
use syntect::parsing::SyntaxSet;
use termion::raw::IntoRawMode;
use termion::{async_stdin, clear, color, cursor, style};

use crate::components::editor::editor_controller::EditorController;
use crate::components::editor::editor_state::EditorState;
use crate::components::editor::editor_view::EditorView;
use crate::globals::style_provider::{FocusType, StyleProvider};
use crate::io::input_event::InputEvent;
use crate::io::keys::Key;
use crate::io::output::Output;
use crate::io::termion_input::TermionInput;
use crate::io::termion_output::TermionOutput;
use crate::layout::default_layout::DefaultLayout;
use crate::layout::dummy_layout::DummyLayout;
use crate::layout::layout::Layout;
use crate::layout::split_layout::{SplitDirection, SplitLayout};
use crate::layout::*;
use crate::primitives::sized_xy::SizedXY;
use crate::primitives::xy::XY;
use crate::rr_switchboard::RrSwitchboard;
use crate::rrc::Rrc;
use crate::rrv::Rrv;
use crate::switchboard::Switchboard;
use crate::view::dummy_view::{DummyView, DummyViewState};
use crate::view::root_view::RootView;
use crate::view::view::View;

mod io;
mod primitives;
mod view;

mod components;
mod experiment;
mod globals;
mod layout;
mod patterns;
mod rr_switchboard;
mod rrc;
mod rrv;
mod switchboard;

fn main() {
    env_logger::builder()
        .filter(None, LevelFilter::Debug)
        .init();

    let ts = ThemeSet::load_defaults();
    let theme = ts.themes.get("base16-eighties.dark").unwrap().clone();

    let style_provider = StyleProvider::new();

    let stdout = stdout();
    let mut stdout = stdout.lock().into_raw_mode().unwrap();
    let stdin = stdin();

    write!(stdout, "{}{}", clear::All, cursor::Goto(1, 1)).unwrap();
    stdout.flush().unwrap();

    let input = TermionInput::new(stdin);
    let mut output = TermionOutput::new(stdout);
    let mut switchboard = RrSwitchboard::new(Switchboard::new());
    let mut root_view = RootView::new(switchboard.clone().into(), output.size());
    root_view.set_size(output.size());

    loop {
        switchboard.borrow_mut().exhaust_ticks(None);

        output
            .update_size()
            .map(|new_size| root_view.set_size(new_size));

        output.clear();

        root_view.render(&style_provider, FocusType::Hovered, &mut output);

        output.end_frame();

        match input.source().recv() {
            Ok(ie) => {
                debug!("{:?}", ie);
                match ie {
                    InputEvent::Tick => {}
                    InputEvent::KeyInput(key) => match key {
                        Key::CtrlLetter('q') => break,
                        _ => {
                            root_view.consume_input(key);
                        }
                    },
                    _ => {}
                }
            }
            Err(e) => {
                debug!("Err {:?}", e);
            }
        }
    }
}
