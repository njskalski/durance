use std::cell::RefCell;
use std::rc::Rc;

pub type Uid = uid::Id<usize>;

pub trait Pilot: Send + Clone {
    type Msg;

    fn send(&self, msg: Self::Msg) -> bool;
}

pub trait Controller {
    fn uid(&self) -> Uid;

    fn wants_tick(&self) -> bool;

    // returns true if tick was actually necessary
    fn tick(&mut self) -> bool;

    fn desc(&self) -> String;
}

pub trait State: Clone {
    type Msg: Send;

    fn update(&mut self, msg: Self::Msg) -> bool;

    fn is_accepting(&self) -> bool;
}
