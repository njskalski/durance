use unicode_segmentation::UnicodeSegmentation;
use unicode_width::UnicodeWidthStr;

pub fn line_screen_width(line: &str) -> u16 {
    line.graphemes(true)
        .fold(0, |acc, grapheme| acc + (grapheme.width() as u16))
}
