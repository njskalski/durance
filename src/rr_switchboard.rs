use crate::switchboard::Switchboard;
use std::borrow::Borrow;
use std::cell::{Ref, RefCell, RefMut};
use std::ops::Deref;
use std::rc::Rc;

pub type AnyCtrl = Rc<RefCell<Switchboard>>;

pub struct RrSwitchboard {
    raw: Rc<RefCell<Switchboard>>,
}

impl RrSwitchboard {
    pub fn new(raw: Switchboard) -> Self {
        RrSwitchboard {
            raw: Rc::new(RefCell::new(raw)),
        }
    }

    pub fn borrow_mut(&self) -> RefMut<Switchboard> {
        self.raw.borrow_mut()
    }

    pub fn borrow(&self) -> Ref<Switchboard> {
        (*self.raw).borrow()
    }
}

impl Clone for RrSwitchboard {
    fn clone(&self) -> Self {
        RrSwitchboard {
            raw: self.raw.clone(),
        }
    }
}
