use std::borrow::Borrow;
use std::cell::{Ref, RefCell, RefMut};
use std::ops::Deref;
use std::rc::Rc;

use crate::patterns::Controller;

pub type AnyCtrl = Rc<RefCell<dyn Controller>>;

pub struct Rrc<T: 'static + Controller> {
    raw: Rc<RefCell<T>>,
}

impl<T: 'static + Controller> Rrc<T> {
    pub fn new(raw: T) -> Self {
        Rrc {
            raw: Rc::new(RefCell::new(raw)),
        }
    }

    pub fn to_any(&self) -> AnyCtrl {
        self.raw.clone() as AnyCtrl
    }

    pub fn borrow_mut(&self) -> RefMut<T> {
        self.raw.borrow_mut()
    }

    pub fn borrow(&self) -> Ref<T> {
        (*self.raw).borrow()
    }
}

impl<T: 'static + Controller> Clone for Rrc<T> {
    fn clone(&self) -> Self {
        Rrc {
            raw: self.raw.clone(),
        }
    }
}
