use std::borrow::Borrow;
use std::cell::{Ref, RefCell, RefMut};
use std::ops::Deref;
use std::rc::Rc;

use crate::view::view::View;

pub type AnyView = Rc<RefCell<dyn View>>;

pub struct Rrv<T: 'static + View> {
    raw: Rc<RefCell<T>>,
}

impl<T: 'static + View> Rrv<T> {
    pub fn new(raw: T) -> Self {
        Rrv {
            raw: Rc::new(RefCell::new(raw)),
        }
    }

    pub fn to_any(&self) -> AnyView {
        self.raw.clone() as AnyView
    }

    pub fn borrow_mut(&self) -> RefMut<T> {
        self.raw.borrow_mut()
    }

    pub fn borrow(&self) -> Ref<T> {
        (*self.raw).borrow()
    }
}

impl<T: 'static + View> From<Rrv<T>> for AnyView {
    fn from(rrv: Rrv<T>) -> Self {
        rrv.to_any()
    }
}

impl<T: 'static + View> Clone for Rrv<T> {
    fn clone(&self) -> Self {
        Rrv {
            raw: self.raw.clone(),
        }
    }
}
