use std::cell::RefCell;
use std::collections::{HashMap, HashSet};
use std::rc::Rc;

use crossbeam_channel::{unbounded, Receiver, Sender, TryRecvError};
use log::debug;
use uid::Id;

use crate::patterns::{Controller, Uid};
use crate::rrc::AnyCtrl;

const HARD_TICK_LIMIT: usize = 1000;

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub enum TickMsgType {
    Tick,
    Drop,
}

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct TickMsg {
    msg_type: TickMsgType,
    uid: Uid,
}

impl TickMsg {
    pub fn new(msg_type: TickMsgType, uid: Uid) -> TickMsg {
        TickMsg { msg_type, uid }
    }
}

pub type TickSender = Sender<TickMsg>;

pub struct Switchboard {
    controllers_set: HashMap<Uid, AnyCtrl>,
    // these two facilitate "tick requests"
    sender: TickSender,
    receiver: Receiver<TickMsg>,
}

impl Switchboard {
    pub fn new() -> Self {
        let (sender, receiver) = unbounded::<TickMsg>();

        Switchboard {
            controllers_set: HashMap::new(),
            sender,
            receiver,
        }
    }

    pub fn insert_controller(&mut self, rrc: AnyCtrl) {
        let uid = rrc.borrow().uid();
        self.controllers_set.insert(uid, rrc);
    }

    pub fn remove_controller(&mut self, rrc: &AnyCtrl) {
        let uid = rrc.borrow().uid();
        self.controllers_set.remove(&uid);
    }

    pub fn get_tick_request_sender(&self) -> &TickSender {
        &self.sender
    }

    pub fn exhaust_ticks(&mut self, limit: Option<usize>) -> bool {
        let limit = limit.unwrap_or(HARD_TICK_LIMIT);
        let mut i: usize = 0;
        let mut loop_cond = true;

        while loop_cond && i < limit {
            if self.tick_once() {
                i += 1;
            } else {
                loop_cond = false;
            }
        }

        i > 0 // returns true if any tick happened
    }

    pub fn tick_once(&mut self) -> bool {
        match self.receiver.try_recv() {
            Ok(msg) => {
                let uid = msg.uid;
                // debug!("got tick req from {}", uid);
                match self.controllers_set.get_mut(&uid) {
                    None => {
                        debug!("controller uid {} not found!", uid);
                        false
                    }
                    Some(ctrl) => {
                        let mut ctrl = ctrl.borrow_mut();
                        debug!("ticking {}", ctrl.desc());
                        let tick_res = ctrl.tick();
                        if !tick_res {
                            debug!("unnecessary tick on uid {}", uid);
                        }
                        true
                    }
                }
            }
            Err(TryRecvError::Empty) => false,
            Err(other) => {
                debug!("error {}", other);
                false
            }
        }
    }
}
