use std::rc::Rc;
use std::sync::Arc;

use log::debug;

use crate::components::puzzles::frame::draw_frame;
use crate::globals::style_provider::{FocusType, StyleProvider};
use crate::io::keys::Key;
use crate::io::output::Output;
use crate::io::style::TextStyle;
use crate::primitives::rect::Rect;
use crate::primitives::xy::XY;
use crate::rrv::AnyView;
use crate::view::view::View;

static MIN_SIZE: XY = XY::new(1, 1);

pub struct DummyViewState {
    current_size: XY,
    accepts_focus: bool,
    focus_type: FocusType,
}

impl DummyViewState {
    pub fn new(accepts_focus: bool) -> DummyViewState {
        DummyViewState {
            current_size: MIN_SIZE,
            accepts_focus,
            focus_type: if accepts_focus {
                FocusType::Alternative
            } else {
                FocusType::Inactive
            },
        }
    }
}

pub struct DummyView {
    state: DummyViewState,
}

impl DummyView {
    pub fn new(state: DummyViewState) -> DummyView {
        DummyView { state }
    }
}

impl View for DummyView {
    fn desc(&self) -> Arc<String> {
        Arc::new("dummy view".into())
    }

    fn min_size(&self) -> XY {
        MIN_SIZE
    }

    fn get_size(&self) -> XY {
        self.state.current_size
    }

    fn set_size(&mut self, new_size: XY) {
        self.state.current_size = new_size;
    }

    fn accepts_input(&self) -> bool {
        self.state.accepts_focus
    }

    fn consume_input(&mut self, key: Key) -> bool {
        false
    }

    fn render(
        &self,
        style_provider: &StyleProvider,
        focus_type: FocusType,
        output: &mut dyn Output,
    ) {
        let style = style_provider.for_text(focus_type);
        // debug!("drawing dummy view {}", self.get_size());

        for x in 0..self.get_size().x {
            for y in 0..self.get_size().y {
                output.print_at((x, y).into(), style, "░");
            }
        }
    }
}
