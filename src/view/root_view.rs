use std::borrow::{Borrow, BorrowMut};
use std::cell::{Cell, RefCell};
use std::rc::Rc;
use std::sync::Arc;

use crate::components::editor::editor_controller::EditorController;
use crate::components::editor::editor_view::EditorView;
use crate::globals::style_provider::{FocusType, StyleProvider};
use crate::io::keys::Key;
use crate::io::output::Output;
use crate::layout::default_layout::DefaultLayout;
use crate::layout::dummy_layout::DummyLayout;
use crate::layout::layout::Layout;
use crate::layout::split_layout::{SplitDirection, SplitLayout};
use crate::primitives::xy::XY;
use crate::rr_switchboard::RrSwitchboard;
use crate::rrc::Rrc;
use crate::rrv::{AnyView, Rrv};
use crate::switchboard::Switchboard;
use crate::view::dummy_view::{DummyView, DummyViewState};
use crate::view::view::View;

pub struct RootView {
    size: XY,
    layout: Box<dyn Layout>,
    switchboard: RrSwitchboard,
    editor_ctrl: Rrc<EditorController>,
    editor_view: Rrv<EditorView>,
    dummy_view: Rrv<DummyView>,
}

impl RootView {
    pub fn new(switchboard: RrSwitchboard, size: XY) -> RootView {
        let editor_ctrl = EditorController::new(switchboard.clone());
        let editor_view = Rrv::new(EditorView::new(switchboard.clone(), editor_ctrl.clone()));
        let dummy_view = Rrv::new(DummyView::new(DummyViewState::new(true)));
        let layout = RootView::build_layout(size, editor_view.clone(), dummy_view.clone());

        RootView {
            size,
            layout,
            editor_ctrl,
            editor_view,
            dummy_view,
            switchboard,
        }
    }

    fn build_layout(size: XY, ev: Rrv<EditorView>, dv: Rrv<DummyView>) -> Box<dyn Layout> {
        Box::new(SplitLayout::new(
            size,
            SplitDirection::Vertical,
            vec![
                (1, Box::new(DefaultLayout::new(dv.into()))),
                (3, Box::new(DefaultLayout::new(ev.into()))),
            ],
            1,
        ))
    }
}

impl View for RootView {
    fn desc(&self) -> Arc<String> {
        Arc::new(format!("RootView"))
    }

    fn min_size(&self) -> XY {
        (30, 20).into()
    }

    fn get_size(&self) -> XY {
        self.size
    }

    fn set_size(&mut self, new_size: XY) {
        self.size = new_size;
        self.layout =
            RootView::build_layout(self.size, self.editor_view.clone(), self.dummy_view.clone());
    }

    fn accepts_input(&self) -> bool {
        true
    }

    fn consume_input(&mut self, key: Key) -> bool {
        match self.layout.active_view() {
            Some(active_view) => (*active_view).borrow_mut().consume_input(key),
            None => false,
        }
    }

    fn render(
        &self,
        style_provider: &StyleProvider,
        focus_type: FocusType,
        output: &mut dyn Output,
    ) {
        self.layout.render(style_provider, focus_type, output)
    }
}
