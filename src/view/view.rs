use std::sync::Arc;

use crate::globals::style_provider::{FocusType, StyleProvider};
use crate::io::keys::Key;
use crate::io::keys_consumer::KeysConsumeResult;
use crate::io::output::Output;
use crate::primitives::rect::Rect;
use crate::primitives::xy::XY;
use crate::rrv::AnyView;

pub trait View {
    //Returns a description of a view
    fn desc(&self) -> Arc<String>;

    fn min_size(&self) -> XY;
    fn get_size(&self) -> XY;
    fn set_size(&mut self, new_size: XY);

    fn accepts_input(&self) -> bool;

    /*
    The current pattern is: first pass it to view's active children, then eventually recurse up.
     */
    fn consume_input(&mut self, key: Key) -> bool;

    fn render(
        &self,
        style_provider: &StyleProvider,
        focus_type: FocusType,
        output: &mut dyn Output,
    );
}
